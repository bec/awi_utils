spec:
  inputs:
    stage:
      default: test
    target:
      default: $CI_PROJECT_NAME
    name:
      default: $CI_PROJECT_NAME
    branch: 
      default: $CI_DEFAULT_BRANCH
    install_bec_lib:
      default: "true"
    install_bec_server:
      default: "true"
    install_bec_ipython_client:
      default: "true"
    install_ophyd_devices:
      default: "true"
    install_bec_widgets:
      default: ""
    

---
# This file is a template, and might need editing before it works on your project.
# Official language image. Look for the different tagged releases at:
# https://hub.docker.com/r/library/python/tags/
image: $CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX/python:3.10

variables:
  BEC_CORE_BRANCH:
    description: bec branch
    value: main
  OPHYD_DEVICES_BRANCH:
    description: ophyd_devices branch
    value: main
  BEC_WIDGETS_BRANCH:
    description: bec_widgets branch
    value: main

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
    - if: $CI_PIPELINE_SOURCE == "web"
    - if: $CI_PIPELINE_SOURCE == "pipeline"
    - if: $CI_PIPELINE_SOURCE == "parent_pipeline"
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH
  auto_cancel:
    on_new_commit: interruptible

include:
  - template: Security/Secret-Detection.gitlab-ci.yml
  - project: "bec/awi_utils"
    file: "templates/device-list-job.yml"
    inputs:
      target: $CI_PROJECT_NAME
      token: $CI_UPDATES



#commands to run in the Docker container before starting each job.
before_script:
  - if [[ "$CI_PROJECT_PATH" != "bec/$[[ inputs.target ]]" ]]; then 
      echo -e "\033[35;1m Using branch $[[ inputs.branch ]] of $[[ inputs.target ]] \033[0;m";
      test -d $[[ inputs.target ]] || git clone --branch $[[ inputs.branch ]] https://oauth2:$CI_RO_TOKEN@gitlab.psi.ch/bec/$[[ inputs.target ]].git; cd $[[ inputs.target ]];
    fi
  - if [ ! -z $[[ inputs.install_bec_lib ]] ]; then
      echo -e "\033[35;1m Installing branch $BEC_CORE_BRANCH of bec-lib \033[0;m";
      if [ ! -d ./bec ]; then
        git clone --branch $BEC_CORE_BRANCH https://gitlab.psi.ch/bec/bec.git;
      fi;
      pip install ./bec/bec_lib;
    fi
  - if [ ! -z $[[ inputs.install_bec_server ]] ]; then
      echo -e "\033[35;1m Installing branch $BEC_CORE_BRANCH of bec-server \033[0;m";
      if [ ! -d ./bec ]; then
        git clone --branch $BEC_CORE_BRANCH https://gitlab.psi.ch/bec/bec.git;
      fi;
      pip install ./bec/bec_server;
    fi
  - if [ ! -z $[[ inputs.install_bec_ipython_client ]] ]; then
      echo -e "\033[35;1m Installing branch $BEC_CORE_BRANCH of bec-ipython-client \033[0;m";
      if [ ! -d ./bec ]; then
        git clone --branch $BEC_CORE_BRANCH https://gitlab.psi.ch/bec/bec.git;
      fi;
      pip install ./bec/bec_ipython_client;
    fi
  - if [ ! -z $[[ inputs.install_ophyd_devices ]] ]; then
      echo -e "\033[35;1m Installing branch $OPHYD_DEVICES_BRANCH of ophyd-devices \033[0;m";
      git clone --branch $OPHYD_DEVICES_BRANCH https://gitlab.psi.ch/bec/ophyd_devices.git;
      pip install ./ophyd_devices;
    fi
  - if [ ! -z $[[ inputs.install_bec_widgets ]] ]; then
      echo -e "\033[35;1m Installing branch $BEC_WIDGETS_BRANCH of bec-widgets \033[0;m";
      git clone --branch $BEC_WIDGETS_BRANCH https://gitlab.psi.ch/bec/bec_widgets.git;
      pip install ./bec_widgets[pyqt6];
    fi
  - pip install -e .[dev]

# different stages in the pipeline
stages:
  - Formatter
  - test # must be called test for security/secret-detection to work
  - AdditionalTests
  - Deploy

pylint:
  stage: Formatter
  script:
    - pip install pylint pylint-exit anybadge
    - pip install -e .[dev]
    - mkdir ./pylint
    - pylint ./$[[ inputs.target ]] --output-format=text --output=./pylint/pylint.log | tee ./pylint/pylint.log || pylint-exit $?
    - PYLINT_SCORE=$(sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' ./pylint/pylint.log)
    - anybadge --label=Pylint --file=pylint/pylint.svg --value=$PYLINT_SCORE 2=red 4=orange 8=yellow 10=green
    - echo "Pylint score is $PYLINT_SCORE"
  artifacts:
    paths:
      - ./pylint/
    expire_in: 1 week
  rules:
    - if: $CI_PROJECT_PATH == "bec/$[[ inputs.target ]]"
  interruptible: true

pylint-check:
  stage: Formatter
  needs: []
  allow_failure: true
  before_script:
    - pip install pylint pylint-exit anybadge
    - pip install -e .[dev]
    - apt-get update
    - apt-get install -y bc
  script:
    # Identify changed Python files
    - git fetch
    - if [ "$CI_PIPELINE_SOURCE" == "merge_request_event" ]; then
        echo origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME;
        TARGET_BRANCH_COMMIT_SHA=$(git rev-parse origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME);
        echo $TARGET_BRANCH_COMMIT_SHA;
        CHANGED_FILES=$(git diff --name-only $SOURCE_BRANCH_COMMIT_SHA $TARGET_BRANCH_COMMIT_SHA | grep '\.py$' || true);
      else
        CHANGED_FILES=$(git diff --name-only $CI_COMMIT_BEFORE_SHA $CI_COMMIT_SHA | grep '\.py$' || true);
      fi
    - if [ -z "$CHANGED_FILES" ]; then echo "No Python files changed."; exit 0; fi

    # Run pylint only on changed files
    - mkdir ./pylint
    - pylint $CHANGED_FILES --output-format=text | tee ./pylint/pylint_changed_files.log || pylint-exit $?
    - PYLINT_SCORE=$(sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' ./pylint/pylint_changed_files.log)
    - echo "Pylint score is $PYLINT_SCORE"

    # Fail the job if the pylint score is below 9
    - if [ "$(echo "$PYLINT_SCORE < 9" | bc)" -eq 1 ]; then echo "Your pylint score is below the acceptable threshold (9)."; exit 1; fi
  artifacts:
    paths:
      - ./pylint/
    expire_in: 1 week
  rules:
    - if: $CI_PROJECT_PATH == "bec/$[[ inputs.target ]]" && $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_PROJECT_PATH == "bec/$[[ inputs.target ]]" && $CI_COMMIT_BRANCH
  interruptible: true

secret_detection:
  before_script:
    - ''

config_test:
  stage: test 
  script:
    - ophyd_test --config ./$[[ inputs.target ]]/device_configs/ --output ./config_tests
  artifacts:
    paths:
      - ./config_tests
    when: on_failure
    expire_in: "30 days"
  interruptible: true

pytest-coverage-3.10:
  stage: test
  script:
    - pip install coverage
    # Check if there are test files in the directory or its subdirectories
    - if [ -n "$(find ./tests -mindepth 1 -type f -name '*.py')" ]; then
    # Run tests only if test files are found
    - coverage run --source=./$[[ inputs.target ]] -m pytest -v --junitxml=report.xml --random-order --full-trace ./tests
    - coverage report
    - coverage xml
    - else
    # Print a message if no test files are found
    - echo "No test files found in the ./tests directory or its subdirectories"
    - touch ./report.xml; touch ./coverage.xml
    - fi
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    reports:
      junit: report.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
  interruptible: true

pytest-3.11:
  stage: test
  image: $CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX/python:3.11
  script:
    # Check if there are test files in the directory or its subdirectories
    - if [ -n "$(find ./tests -mindepth 1 -type f -name '*.py')" ]; then
    # Run tests only if test files are found
    - pytest -v --random-order ./tests
    - else
    # Print a message if no test files are found
    - echo "No test files found in the ./tests directory or its subdirectories"
    - fi
  interruptible: true

pytest-3.12:
  extends: "pytest-3.11"
  image: $CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX/python:3.12