import json
import urllib.request

from packaging.specifiers import SpecifierSet
from packaging.version import Version


def fetch_dependencies(package_name, version):
    """Fetch the dependencies of a specific package version from PyPI."""
    url = f"https://pypi.org/pypi/{package_name}/{version}/json"
    with urllib.request.urlopen(url) as response:
        if response.status == 200:
            package_data = json.loads(response.read().decode())
            return package_data.get("info", {}).get("requires_dist", [])
        else:
            print(f"Failed to fetch data for {package_name}=={version}")
            return []


def parse_dependency_string(dependency_string):
    """Parse dependency strings into name and version specifier."""
    if ";" in dependency_string:
        dependency_string = dependency_string.split(";")[
            0
        ].strip()  # Ignore environment markers for simplicity
    parts = dependency_string.split(" ")
    package_name = parts[0]
    version_specifier = " ".join(parts[1:]) if len(parts) > 1 else ""
    return package_name, SpecifierSet(version_specifier)


def fetch_all_versions(package_name):
    """Fetch all available versions of a package from PyPI."""
    url = f"https://pypi.org/pypi/{package_name}/json"
    with urllib.request.urlopen(url) as response:
        if response.status == 200:
            package_data = json.loads(response.read().decode())
            return list(package_data["releases"].keys())
        else:
            print(f"Failed to fetch data for {package_name}")
            return []


def resolve_dependencies(package_name, constraint_specifier, resolved=None, to_resolve=None):
    """Resolve all dependencies recursively and ensure compatibility across versions."""
    if resolved is None:
        resolved = {}
    if to_resolve is None:
        to_resolve = {}

    # Fetch and filter available versions based on constraints
    all_versions = fetch_all_versions(package_name)
    compatible_versions = [ver for ver in all_versions if Version(ver) in constraint_specifier]

    # Use the latest compatible version for simplicity (could implement more complex selection if needed)
    if not compatible_versions:
        print(
            f"No compatible versions found for {package_name} with constraints {constraint_specifier}"
        )
        return resolved
    chosen_version = compatible_versions[-1]  # Use latest compatible version

    # Resolve direct dependencies of this version
    direct_dependencies = fetch_dependencies(package_name, chosen_version)
    resolved[package_name] = chosen_version

    for dep in direct_dependencies:
        dep_name, dep_specifier = parse_dependency_string(dep)

        # Check if already resolved
        if dep_name in resolved:
            # Verify compatibility if it’s already resolved
            if Version(resolved[dep_name]) not in dep_specifier:
                print(
                    f"Incompatible dependency version for {dep_name}: {resolved[dep_name]} vs. {dep_specifier}"
                )
            continue

        # Add to pending resolution if not already being processed
        if dep_name not in to_resolve:
            to_resolve[dep_name] = dep_specifier
            resolve_dependencies(dep_name, dep_specifier, resolved, to_resolve)

    return resolved


if __name__ == """__main__""":
    resolved_dependencies = resolve_dependencies("numpy", SpecifierSet("~=1.0"))
    print(resolved_dependencies)
