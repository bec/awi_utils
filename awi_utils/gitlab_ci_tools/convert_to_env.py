"""
This script converts a subset of yaml file to env variables
"""

import os
import sys

import yaml


def convert_to_env(yaml_file: str, branch_name: str):
    """
    Convert a subset of yaml file to env variables
    """
    if not os.path.exists(yaml_file):
        print(f"File {yaml_file} not found")
        sys.exit(0)
    with open(yaml_file, "r", encoding="utf-8") as stream:
        try:
            data = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
    if branch_name not in data:
        print(f"Branch {branch_name} not found in yaml file")
        sys.exit(0)
    data = data[branch_name]
    with open("build.env", "w", encoding="utf-8") as build_env_file:
        for key, value in data.items():
            print(f"Using {key}={value}")
            build_env_file.write(f"{key}={value}\n")


if __name__ == "__main__":

    if len(sys.argv) != 3:
        print("Usage: python convert_to_env.py <yaml_file> <branch_name>")
        sys.exit(1)

    file_in = sys.argv[1]
    target = sys.argv[2]
    convert_to_env(file_in, target)
