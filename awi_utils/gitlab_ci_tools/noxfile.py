""" This script is used by the CI to check dependencies, 
and run tests with minimum required package versions."""

import json
import os
from contextlib import redirect_stdout
from io import StringIO

import nox
from packaging.version import Version
from pip import main as pipmain

from awi_utils.gitlab_ci_tools.gitlab_handler import GitlabHandler
from awi_utils.gitlab_ci_tools.package_version_handler import PackageVersionHandler

GITLAB_API = None
if os.getenv("CI_UPDATES", None):
    GITLAB_API = GitlabHandler(os.getenv("CI_UPDATES", None))

# Create a log file directory
log_dir = os.path.abspath("test_pkg_versions_logs")
os.makedirs(log_dir, exist_ok=True)

# Path to the pyproject.toml file
# Assuming that the job is started from within the folder where the pyproject.toml file is located
toml_filepath = os.path.abspath("pyproject.toml")


class DependencyCheckConflictError(Exception):
    """Raised when a dependency conflict is detected during the test"""


class DependencyCheckTestError(Exception):
    """Raised when an error occurs during the test"""


@nox.session
def test_and_write_log(session) -> None:
    """

    This nox session will pip install the pyproject.toml file with the provided pip_args (default'.[dev]'),
    and run pytest with the provided pytest_args (default is '-v,--random-order,./tests/').
    The output of this will be written to a log file.

    First argument is the version of the dependencies to test.
    Second argument is the pytest arguments to pass to pytest.
    Third argument is the pip arguments to pass to pip install.

    Example usage:
    nox -s test_and_write_log -- min -v,--random-order,./tests/ '.[dev,pyside6]'

    -> logs will be written to test_log_min_version.txt
    """
    version = session.posargs[0]
    pytest_args = session.posargs[1] if len(session.posargs) > 1 else None
    pip_args = session.posargs[2] if len(session.posargs) > 2 else None
    if pytest_args is None:
        pytest_args = ["-v", "--random-order", "./tests/"]
    else:
        pytest_args = pytest_args.split(",")
    if not pip_args:
        pip_args = ".[dev]"
    log_file_name = os.path.join(log_dir, f"test_log_{version.lower()}_version.txt")
    with open(log_file_name, "w+", encoding="utf-8") as f:
        f.write(f"Running tests with {version} versions of dependencies")
        f.write("\n-----------------------------------------\n")
        f.write("\n-----------------------------------------\n")
        f.flush()
        session.run("pip", "install", f"{pip_args}", silent=False, stdout=f)
        f.flush()
        session.run("pytest", *pytest_args, silent=False, stdout=f)


def parse_session_args(posargs: list[str]):
    """Parse the session arguments and return the pytest_args, ignore_args, and pip_args.

    Args:
        posargs (list[str]): The positional arguments passed to the session.
    """
    pytest_args = None
    ignore_args = None
    pip_args = None

    for arg in posargs:
        if arg.startswith("pytest_args"):
            pytest_args = arg.split("=")[1]
            continue
        if arg.startswith("ignore_dep_group"):
            ignore_args = arg.split("=")[1]
            continue
        if arg.startswith("pip_args"):
            pip_args = arg.split("=")[1]
    return pytest_args, ignore_args, pip_args


def install_and_log_dependency(
    session: nox.session, package: str, version: str, log_fname: str
) -> dict:
    """
    Utility method that receives a nox session, a package name, and a version.
    It will try to install the package with the specified version while capturing
    the log output. It will screen the logs for any ERROR and 'Attempting uninstall' lines,
    and return these logs including the full log in form of a dictionary.

    Args:
        session (nox.session): The nox session object.
        package (str): The name of the package to install.
        version (str): The version of the package to install.

    """
    try:
        dependency = f"{package}=={version}"
        with open(log_fname, "a", encoding="utf-8") as f:
            session.run("pip", "install", dependency, stdout=f)
    except nox.command.CommandFailed as e:
        print(f"Error installing {package}=={version} in fresh environment. Exception raise {e}")


@nox.session
def pytest_check_versions(session):
    """This nox session is using the PackageVersionHandler to open the pyproject.toml file
    of the current folder, and receives a version argument through the session.posargs.
    It will filter the dependencies based on the version argument ('min', 'max', 'random'),
    to specific install candidates, and install each dependency using pip install dependency==version.

    We will log the output of the installation process to a log file, and create a new pyproject.toml file
    with the resolved dependencies. This job should run in preparation for the test_and_write_log session,
    which will run the unit tests of the repository with the fixed dependencies.
    """
    if getattr(session, "posargs", None) is not None:
        args = session.posargs
    else:
        print("No arguments passed to the session, using max version as default")
        args = ["max"]
    version = args[0]
    ignore_args = args[1] if len(args) > 1 else None

    log_fname = os.path.join(log_dir, f"Install_log_{version.lower()}_version.txt")
    with open(log_fname, "w+", encoding="utf-8") as f:
        f.write("Running pytest check version")
        f.write("\n-----------------------------------------\n")

    if not ignore_args:
        ignore_args = None
    with PackageVersionHandler(
        toml_filepath=toml_filepath, include_optional_dep=True, ignore_dependency_groups=ignore_args
    ) as handler:
        # Get the dependencies for the specified version
        dependencies = handler._select_versions(version.lower())
        # Run installation and logging for each dependency via pip install
        # Raise an exception if an error occurs during installation
        for _, deps in dependencies.items():
            for name, vers in deps.items():
                print(f"Installing dependency {name}: {vers}")
                install_and_log_dependency(session, name, str(vers), log_fname=log_fname)
        tmp_file = os.path.join(log_dir, f"pip_freeze_{version}.txt")
        with open(tmp_file, "w", encoding="utf-8") as f:
            session.run("pip", "freeze", stdout=f)
        with open(tmp_file, "r", encoding="utf-8") as f:
            for line in f.readlines():
                name, vers = line.split("==")
                vers = Version(vers.strip("\n").strip())
                for dep_group, deps in dependencies.items():
                    if name in deps:
                        if vers != deps[name]:

                            # Check if the update is possible from the initial dependencies
                            if handler.check_if_candidate_is_valid(name, vers) is False:
                                raise DependencyConflictError(
                                    f"Dependency conflict for {name} with specified version {deps[name]} and updated version {vers}"
                                )
                            print(f"Updating dependency for {name} from {deps[name]} to {vers}")

                            deps[name] = vers
        # Update the resolved dependencies with the final versions
        toml = handler.get_project_toml()
        for name, deps in dependencies.items():
            if name == "core":
                toml["project"]["dependencies"] = [f"{n}=={v}" for n, v in deps.items()]
                continue
            toml["project"]["optional-dependencies"][name] = [f"{n}=={v}" for n, v in deps.items()]
        # Write temporary toml file with version attached
        handler.write_toml_file(filepath=toml_filepath, toml_dict=toml)
        with open(log_fname, "a", encoding="utf-8") as f:
            f.write("Installing dependencies from requirements in toml file:")
            f.write("\n-----------------------------------------\n")
            json.dump(toml, f, indent=4)
            f.write("\n-----------------------------------------\n")


@nox.session
def test_outdated_packages(session):
    """
    This nox session will check for outdated packages in the current environment,
    and compare them to the most recent version available on PyPI.

    It will write them to a log file "test_outdated_packages" in the log directory.
    """
    session.run("pip", "cache", "purge")
    session.run("pip", "install", "--upgrade", ".[dev]")
    parser = StringIO()
    with redirect_stdout(parser):
        pipmain(["list", "--outdated"])

    with PackageVersionHandler(toml_filepath=toml_filepath, include_optional_dep=True) as handler:
        installed_dep = {}
        most_recent_dep = {}
        for line in parser.getvalue().split("\n"):
            if line.startswith("Package") or line.startswith("-----"):
                continue
            if not line:
                continue
            line = line.split()
            for dep_group, candidates in handler._available_candidates.items():
                for req, _ in candidates.items():
                    if line[0] == req.name:
                        installed_dep[req.name] = line[1]
                        most_recent_dep[req.name] = line[2]
        if not installed_dep:
            print("No outdated packages found")
            return
        with open(os.path.join(log_dir, "test_outdated_packages"), "w+", encoding="utf-8") as f:
            print("Summary of outdated packages")
            print("\n-----------------------------------------\n")
            f.write("Summary of outdated packages")
            f.write("\n-----------------------------------------\n")
            for key, value in installed_dep.items():
                rtr = f"{key}: {value} -> {most_recent_dep[key]} \n"
                print(rtr)
                f.write(rtr)
                if GITLAB_API is None:
                    continue
                gitlab_issue_name = f"Outdated package found for {key}: {value}"
                description_note = f"Installed version for package {key}: {value} \n\n Most recent version for package {key}: {most_recent_dep[key]}"
                description = f'# Summary \n\n Pipeline: {os.getenv("CI_PIPELINE_URL", None)} failed due to outdated package {key} \n\n # Description \n\n {description_note}'
                try:
                    user_ids = [
                        GITLAB_API.get_userid_from_name(os.getenv("GITLAB_USER_NAME", None))
                    ]
                except:
                    user_ids = None

                GITLAB_API.create_gitlab_issue(
                    name=gitlab_issue_name, description=description, user_ids=user_ids
                )


@nox.session
@nox.parametrize("version", ["min", "random", "max"])
def main(session, version):
    """Run tests with the specified version of the dependencies.

    Args:
        version: The version to test. Can be 'min', 'max', or 'random'.
        pytest_args: Additional arguments to pass to pytest.
        ignore_args: List of dependency groups to exclude from the test.
    """
    print(f"Running nox session with {version} version of for package toml {toml_filepath} \n")
    with PackageVersionHandler(toml_filepath=toml_filepath, include_optional_dep=True) as handler:
        original_toml = handler.get_project_toml()
        if getattr(session, "posargs", None) is not None:
            args = session.posargs
        else:
            args = ""
        pytest_args, ignore_args, pip_args = parse_session_args(args)
        if pytest_args is None:
            pytest_args = "-v,--random-order,./tests/"
        if ignore_args is None:
            ignore_args = ""
        if pip_args is None:
            pip_args = ""
        print("Running pytest check version")
        # Resolve and log dependencies for the specified version
        try:
            session.run(
                "nox",
                "-s",
                "pytest_check_versions",
                "--",
                f"{version}",
                f"{ignore_args}",
                external=True,
            )
        except (Exception, KeyboardInterrupt) as e:
            print(f"Error during pytest check version: {e}")
            handler.write_toml_file(toml_filepath, original_toml)
            raise DependencyCheckConflictError(f"Error during pytest check version: {e}") from e

        # Run test with frsh environment based on the resolved dependencies
        try:
            session.run(
                "nox",
                "-s",
                "test_and_write_log",
                "--",
                f"{version}",
                f"{pytest_args}",
                f"{pip_args}",
                external=True,
            )
        except (Exception, KeyboardInterrupt) as e:
            print(f"Error during test_and_write_log: {e}")
            handler.write_toml_file(toml_filepath, original_toml)
            raise DependencyCheckTestError(f"Error during test_and_write_log: {e}") from e

        # Write the initial toml file back to the original file
        handler.write_toml_file(toml_filepath, handler.get_project_toml())
    if session == "max":
        try:
            session.run("nox", "-s", "test_outdated_packages", external=True)
        except (Exception, KeyboardInterrupt) as e:
            print(f"Error during test_outdated_packages: {e}")
            handler.write_toml_file(toml_filepath, original_toml)
            raise DependencyCheckTestError(f"Error during test_outdated_packages: {e}") from e
