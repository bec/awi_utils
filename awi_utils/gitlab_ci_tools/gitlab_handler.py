import os

from gitlab import Gitlab


class GitlabHandler:
    """A class to handle gitlab api calls.

    This class can be used to communicate with Gitlab API within a CI_Pipeline"""

    def __init__(self, privat_token: str):
        self.gl = Gitlab(
            os.getenv("CI_SERVER_URL", "https://gitlab.psi.ch"), private_token=privat_token
        )
        self.project = None
        if os.getenv("CI_PROJECT_ID", None):
            self.set_project(os.getenv("CI_PROJECT_ID"))

    def set_project(self, project_id: int):
        """Set the project."""
        self.project = self.gl.projects.get(project_id)

    def get_userid_from_name(self, username: str):
        """Get the user id from the name."""
        users = self.gl.users.list(username=username)
        if users:
            return users[0].id
        else:
            print(f"User '{username}' not found.")
            return None

    def create_gitlab_issue(
        self, name: str, description: str, user_ids: list, labels: list[str] = None
    ):
        """Create a gitlab issue.

        If the issue already exists, return immediately.

        Args:
            name (str): The name of the package.
            description (str): The description of the issue.
            user_ids (list): The list of user ids to assign the issue to.
        """
        gitlab_issue_name = name
        if self.check_if_issue_exists(gitlab_issue_name):
            print(f"Issue with {name} already exists, returning")
            return
        if labels is None:
            labels = ["issue::reproducible", "priority::needed", "scope::backend", "stage::pending"]

        issue_details = {
            "title": gitlab_issue_name,
            "description": description,
            "assignee_ids": user_ids,
            "labels": labels,
        }
        self.project.issues.create(issue_details)

    def check_if_issue_exists(self, name: str):
        """Check if an issue with the same name exists in the project.

        Args:
            name (str): The name of the issue.
        """
        issues = self.project.issues.list()
        for issue in issues:
            if issue.title == name:
                return True
        return False
