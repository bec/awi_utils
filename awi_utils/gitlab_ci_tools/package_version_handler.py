""" Module for handling package versions from pyproject.toml file. """

import enum
import os
import random
from copy import deepcopy
from typing import DefaultDict

import tomlkit.api
from packaging.requirements import Requirement
from packaging.version import Version
from piptools.repositories.pypi import PyPIRepository
from tomlkit.toml_document import TOMLDocument


class Operation(str, enum.Enum):
    """Operator class."""

    MIN = "min"
    MAX = "max"
    RANDOM = "random"


class PackageVersionHandler:
    """PackageVersionHandler class."""

    def __init__(
        self,
        toml_filepath: str,
        include_optional_dep: bool = True,
        ignore_dependency_groups: list[str] | None = None,
    ):
        """Initialize the PackageVersionHandler class.

        Ths class should be used as a context manager

        This class is used to load available package versions from PyPI
        based on the dependencies in the pyproject.toml file.
        It can provide the minimum required versions for each package,
        the maximum required versions for each package or a random
        distribution of versions for each package used for CI pipelines.

        Args:
            toml_filepath (str): Path to the pyproject.toml file.
            include_optional_dep (bool): Include optional dependencies. Defaults to True.

        Example:
        >>> with PackageVersionHandler(toml_file="./pyproject.toml") as handler:
        >>>     toml_min = handler.get_toml_min_req()
        >>>     toml_latest = handler.get_toml_latest_req()
        >>>     toml_random = handler.get_toml_random_req()
        >>>     handler.write_toml_file("requirements_min.txt", min_req)

        """
        self.repo = PyPIRepository(pip_args=[""], cache_dir="")
        self._toml_filepath = toml_filepath
        self._project_toml_save_copy = None
        self._available_candidates = {}
        self._pkg_dep = {}
        self._include_optional_dep = include_optional_dep
        if ignore_dependency_groups is None:
            ignore_dependency_groups = []
        self._ignore_dependency_groups = ignore_dependency_groups

    def __enter__(self):
        self.run()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        pass

    def get_project_toml(self):
        """Create copy from the save_copy of the project toml file."""
        return deepcopy(self._project_toml_save_copy)

    @property
    def available_candidates(self) -> dict:
        """Returns dictionary with available candidates for each package."""
        return self._available_candidates

    def _select_versions(self, operation: Operation) -> dict[str, list[str]]:
        """Select versions based on the input argument.

        Args:
            operation (Literal["min", "max", "random"]): Operation to perform on the available candidates.
        """
        if operation == Operation.MIN:
            op = min
        elif operation == Operation.MAX:
            op = max
        elif operation == Operation.RANDOM:
            op = random.choice
        else:
            raise ValueError(
                f"Invalid operation: {operation}. Choose from 'min', 'max' or 'random'."
            )
        dependencies = DefaultDict(lambda: {})
        for name, dep in self._available_candidates.items():
            if name in self._ignore_dependency_groups:
                continue
            for dep_name, dep_version in dep.items():
                version = op(dep_version)
                dependencies[name][dep_name] = version
        return dependencies

    def load_toml_file(self) -> None:
        """load toml file and return the content as a dictionary."""
        filepath = os.path.abspath(self._toml_filepath)
        with open(filepath, "r", encoding="utf-8") as f:
            self._project_toml_save_copy = tomlkit.api.load(f)

    def write_toml_file(self, filepath: str, toml_dict: TOMLDocument) -> None:
        """Save toml file to disk."""
        filepath = os.path.abspath(filepath)
        with open(filepath, "w", encoding="utf-8") as f:
            tomlkit.api.dump(toml_dict, f)

    def get_package_dependencies_from_toml(self, include_optional_dep: bool = True) -> dict:
        """Gets the package dependencies from the pyproject.toml file.

        Args:
            filepath (str): Path to the pyproject.toml file.
            include_optional_dep (bool): Include optional dependencies. Defaults to True.
        """
        project_toml = self.get_project_toml()
        self._pkg_dep["core"] = project_toml["project"]["dependencies"]
        if include_optional_dep:
            if "optional-dependencies" not in project_toml["project"]:
                return self._pkg_dep
            for name, dep in project_toml["project"]["optional-dependencies"].items():
                if name in self._ignore_dependency_groups:
                    continue
                self._pkg_dep[name] = dep
        return self._pkg_dep

    def get_available_candidates(self, pkg_dep: dict) -> dict:
        """Update available candidates from dependency list.

        Args:
            dependency_list (list[str]): List of dependencies as parsed from pyproject.toml file.
        """
        rtr = {}
        for name, dep in pkg_dep.items():
            rtr[name] = {}
            for req_str in dep:
                rtr[name].update(self.update_candidate(req_str))
        return rtr

    def check_if_candidate_is_valid(self, pkg_name: str, version: Version) -> bool:
        """Check if the candidate is valid.

        Args:
            pkg_name (str): Package name.
            version (Version): Version of the package.

        Returns:
            bool: True if the candidate is valid, False otherwise, also if it is not found.
        """
        for dep_group, deps in self._available_candidates.items():
            if pkg_name in deps:
                return version in deps[pkg_name]
            return False

    def update_candidate(self, req_str: str) -> dict:
        """Update candidate information in self._available_candidates.

        Args:
            req_str (str): Requirement string in the format from toml.
        """
        ireq = Requirement(req_str)
        all_candidates = set(
            [
                Version(candidate.version.public)
                for candidate in self.repo.find_all_candidates(ireq.name)
            ]
        )
        return {ireq.name: sorted(list(ireq.specifier.filter(all_candidates, prereleases=False)))}

    def run(self) -> None:
        """Run the package version handler."""
        self.load_toml_file()
        self._pkg_dep = self.get_package_dependencies_from_toml(
            include_optional_dep=self._include_optional_dep
        )
        self._available_candidates = self.get_available_candidates(self._pkg_dep)
