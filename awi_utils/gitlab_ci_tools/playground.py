import enum
import json
import random
import threading
import time
import urllib
from concurrent.futures import ThreadPoolExecutor, as_completed
from functools import lru_cache

from packaging.requirements import Requirement, SpecifierSet
from packaging.version import Version
from piptools.repositories.pypi import PyPIRepository


class CombineRequirementsError(Exception):
    """Error raised when combining requirements fails."""


class Operation(str, enum.Enum):
    """Operator class."""

    MIN = "min"
    MAX = "max"
    RANDOM = "random"


class ResolveDependencies:
    def __init__(self, repo: PyPIRepository | None = None):
        if repo is None:
            repo = PyPIRepository(pip_args=[""], cache_dir="")
        self.repo = repo
        self.exclude = [
            "setuptools",
            "pip",
            "wheel",
            "packaging",
            "pyparsing",
            "six",
            "distutils",
            "importlib-metadata",
            "importlib-resources",
        ]
        self.local_data = threading.local()

    def fetch_requirements(self, req: Requirement) -> dict[str, dict]:
        """Fetch the dependencies of a specific package version from PyPI."""
        if not hasattr(self.local_data, "visited"):
            self.local_data.visited = set()  # Initialize visited only once per thread

        visited = self.local_data.visited

        package_name = req.name
        versions = self.get_versions(req)
        rtr = {}
        rtr[package_name] = {}
        if package_name in self.exclude:
            rtr[package_name]["excluded"] = {"req": req, "sub_deps": None}
            return rtr
        for v in versions:
            print(f"Fetching dependencies for {package_name}=={v}")
            package_version_tuple = (package_name, v)
            # Skip this package version if it has already been processed
            if package_version_tuple in visited:
                print(f"Skipping {package_name}=={v} as it has already been processed")
                continue
            deps = self.get_sub_dependencies(package_name=package_name, version=v)
            if len(deps) == 0:
                rtr[package_name][v.public] = {"req": req, "sub_deps": None}
            else:
                sub_deps = {}
                for dep in deps:
                    nested_deps = self.fetch_requirements(dep)
                    sub_deps.update(nested_deps)
                rtr[package_name][v.public] = {"req": req, "sub_deps": sub_deps}

        return rtr

    @lru_cache(maxsize=50000)
    def get_versions(self, req: Requirement) -> list[Version]:
        try:
            candidates = set(
                [
                    candidate
                    for candidate in self.repo.find_all_candidates(req.name)
                    if getattr(candidate, "version", None) is not None
                ]
            )
            out = []
            for candidate in candidates:
                try:
                    out.append(Version(candidate.version.public))
                except Exception as e:
                    print(f"Failed to retrieve versions for {req} and {candidate}: {e}")
            if len(out) == 0:
                print(f"No versions found for {req}")
                return []
            return sorted(list(req.specifier.filter(set(out), prereleases=False)))
        except Exception as e:
            print(f"Failed to retrieve versions for {req}: {e}")
            return []

    @lru_cache(maxsize=50000)
    def get_sub_dependencies(self, package_name: str, version: str) -> list[Requirement]:
        """Getting the dependency for a specific version of a package.

        Args:
            package_name (str): Name of the package.
            version (str): Version of the package.

        Returns:
            tuple: Tuple with version and list of dependencies.
        """
        url = f"https://pypi.org/pypi/{package_name}/{version}/json"
        try:
            with urllib.request.urlopen(url) as response:
                if response.status == 200:
                    package_info = json.loads(response.read().decode())
                    raw_dependencies = package_info["info"].get("requires_dist", None)
                    if raw_dependencies is None:
                        return []
                    # Process dependencies to filter out extras
                    dependencies = []
                    for dep in raw_dependencies:
                        try:
                            if "extra" in dep:
                                continue
                            req = Requirement(dep)
                            # Exclude dependencies with extras
                            if req.marker and "extra" in str(req.marker):
                                continue
                            if not req.extras:
                                dependencies.append(req)
                        except Exception as e:
                            print(f"Failed to parse dependency '{dep}': {e}")
                    return dependencies
                else:
                    return []
        except Exception as e:
            print(f"Error fetching {package_name}=={version}: {e}")
        return []

    def run(self, vers_reqs: list[Requirement], deps: list[Requirement]) -> dict[str, Requirement]:
        return_dict = {}
        data = {}
        with ThreadPoolExecutor(max_workers=10) as executor:
            futures = {executor.submit(self.fetch_requirements, req): req for req in vers_reqs}
            for future in as_completed(futures):
                data.update(future.result())
        # Update the requirements with the sub dependencies
        updates = dict((req.name, []) for req in deps)
        for req in deps:
            conditions = self._resolve_matching_sub_deps([req.name for req in deps], data[req.name])
            for condition in conditions:
                pkg = condition[0]
                updated_req = condition[1]
                if pkg is not None:
                    # This is because the logic is a bit weird, could be improved
                    # Ignore the dependency if it is already specified via vers_reqs
                    if updated_req in vers_reqs:
                        continue
                    updates[pkg].append(updated_req)

        # Combine the requirements from sub dependencies into a single requirement
        for pkg, reqs in updates.items():
            if len(reqs) == 0:
                continue
            return_dict[pkg] = combine_requirements(reqs)

        return return_dict

    def _resolve_matching_sub_deps(
        self, pkgs: list[str], sub_deps: dict | None
    ) -> tuple[str, Requirement] | tuple[None, None]:
        rtr = []
        for v, sub_dep in sub_deps.items():
            if sub_dep["sub_deps"] is None:
                if sub_dep["req"].name in pkgs:
                    return [(sub_dep["req"].name, sub_dep["req"])]
                return [(None, None)]
            for name, sub in sub_dep["sub_deps"].items():
                rtr.extend(self._resolve_matching_sub_deps(pkgs, sub))
        return rtr

        # return self._resolve_matching_sub_deps(pkgs, sub)


def combine_requirements(reqs: list[Requirement]) -> Requirement:
    """Combine multiple requirements into a single requirement.
    If only a single requirement is given, it will return the same requirement.
    If requirements for different packages are given, it will raise a NameError.
    """
    if len(reqs) == 0:
        raise ValueError("No requirements given.")
    if len(reqs) == 1:
        return reqs[0]
    if any([req.name != reqs[0].name for req in reqs]):
        raise NameError(f"Requirements {reqs} must all be from the same package: {reqs[0].name} ")
    if all([req.specifier == SpecifierSet("") for req in reqs]):
        return reqs[0]

    specifier_str = [str(req.specifier) for req in reqs]
    if len(set(specifier_str)) == 1:
        combined_specifiers = SpecifierSet(specifier_str[0])
    else:
        combined_specifiers = SpecifierSet(",".join(specifier_str))
    if not combined_specifiers:
        raise CombineRequirementsError(
            f"Failed to combine requirements {reqs} for package {reqs[0].name}"
        )
    return Requirement(f"{reqs[0].name}{combined_specifiers}")


def select_version(
    repo: PyPIRepository, pkgs: list[Requirement], operation: Operation
) -> list[Requirement]:
    if operation == Operation.MIN:
        op = min
    elif operation == Operation.MAX:
        op = max
    elif operation == Operation.RANDOM:
        op = random.choice
    else:
        raise ValueError(f"Invalid operation: {operation}")
    out = []
    for req in pkgs:
        versions = get_versions(repo=repo, req=req)
        if versions is None:
            raise ValueError(f"Failed to find version for {req}")
        version = op(versions)
        new_req = Requirement(f"{req.name}=={version.public}")
        out.append(new_req)
    return out


def get_versions(repo: PyPIRepository, req: Requirement) -> list[Version]:
    """Get the versions of a package."""
    candidates = set(
        [
            candidate
            for candidate in repo.find_all_candidates(req.name)
            if getattr(candidate, "version", None) is not None
        ]
    )
    out = []
    for candidate in candidates:
        try:
            out.append(Version(candidate.version.public))
        except Exception as e:
            print(f"Failed to retrieve versions for {req} and {candidate}: {e}")
    if len(out) == 0:
        print(f"No versions found for {req}")
        return []
    return sorted(list(req.specifier.filter(set(out), prereleases=False)))


if __name__ == "__main__":
    resolver = ResolveDependencies()
    # The original dependencies
    deps = [
        # "ophyd ~= 1.9", # Run it first without ophyd to test
        "typeguard ~= 4.1, >=4.1.5",
        "prettytable ~= 3.9",
        # "bec_lib ~= 2.12, >=2.12.2", # Run it first without bec_lib to test it
        "numpy >= 1.24, <3.0",
        "pyyaml ~= 6.0",
        "std_daq_client ~= 1.3",
        "pyepics ~= 3.5, >=3.5.5",
        "pytest ~= 8.0",
        "h5py ~= 3.10",
        "hdf5plugin ~= 4.3",
    ]

    # The dependencies from pyproject.toml
    deps = [Requirement(v) for v in deps]
    # Requirement for op versions and applying the operation
    op = Operation.MIN
    # The "op" version for the dependencies
    vers_reqs = select_version(repo=resolver.repo, pkgs=deps, operation=op)
    start_time = time.time()
    # to be updated requirements
    updated_reqs = resolver.run(vers_reqs, deps)
    print(f"Time taken: {time.time() - start_time}")
    print(updated_reqs)
    # Now we simply have to update the dependencies and reapply the opeation
