# AWI utils

This package offers various Python utilities for tasks such as file loading, file saving, and data inspection.

## Gitlab_ci_tools

The submodule `gitlab_ci_tools` provides utility tools and scripts helpful when interacting with Gitlab CI/CD pipelines.

## Templates
Within the `templates` folder, we currently offer 2 templates for CI pipelines.

### plugin-repo-template.yml
This template is a *pipeline template* designed to be embedded in CI pipelines. 
Its purpose is to facilitate easy testing of the *beamline plugin* repositories of `BEC`, i.e. *csaxs_bec*, *tomcat_bec*, *debye_bec*, ...
To embed the template in your CI/CD pipeline, you can simply add the following code to your `.gitlab-ci.yml` file. 
The example is shown for the *csaxs_bec* repository.

``` yaml
include:
  - project: bec/awi_utils
    file: /templates/plugin-repo-template.yml
    inputs:
      name: "csaxs"
      target: "csaxs_bec"
```
Note: [*Pipeline templates*]](https://docs.gitlab.com/ee/development/cicd/templates.html) can not include other *pipeline templates* using *include*.

### check-package-job.yml
This template is a *ci-job template*. 
The purpose of this job is to be included in your pipeline for testing the specified dependencies of your package using *MIN*, *MAX* and a *RANDOM* ranges as defined in your *pyproject.toml* file. 
Additionally, it checks whether your dependencies are outdated, and newer packages are available.
The following points are assumed:
- Your project has a *pyproject.toml* configuration file
- The job will only be triggered if the env variable `CHECK_PKG_VERSIONS == "1"` is set
- You are using `pytest` for unit testing
- For the *outdated packages job*, *Gitlab issues* can be created automatically. However, the job assumes the env variable `CI_UPDATES` to provide a Gitlab api token for the project that is triggering the job

To include the job in your project, you can add the following to your *.gitlab-ci.yml* file:

``` yaml
  - project: "bec/awi_utils"
    file: "/templates/check-packages-job.yml"
    inputs:
      stage: test # default 'test'
      path: "<relative_path_to_pyproject.toml>" # default '.'
      ignore_dep_group: "pyqt6" # default empty
      pytest_args: "-v,--random-order,tests/" # default -v,--random-order,tests/
      pip_args: "[dev,pyside6]" # default '[dev'] 
```
- *Path* can be used to specify a relative path in case your project has multiple sub repositories, i.e. *bec* with *bec_lib*, *bec_server* and *bec_ipython_client*
- *pytest_args* will be passed, and the job runs pytest with the specified args.
- *ignore_dep_group* can be used to specify certain optional dependency groups that should be excluded from the dependency versions. This can be used for pyside6/pyqt6 dependencies which would not be installed together. 
- *pip_args* can be used to specify additional pip arguments for the job. It will be passed to the `pip install` command and can be used to install specific optional dependencies,i.e. '[dev,pyside6]'.

Note: Ci pipelines can add multiple *ci-job templates*, but it is recommended not to use any globals such as *image* or *before_script*, as this may overwrite and affect the behavior of the pipeline triggering the job. This is a clear difference to *pipeline templates*.