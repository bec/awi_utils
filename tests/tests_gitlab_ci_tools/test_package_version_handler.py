from collections import namedtuple
from unittest import mock

import pytest
from packaging.version import Version

from awi_utils.gitlab_ci_tools.package_version_handler import PackageVersionHandler


@pytest.fixture
def package_version_handler():
    with (
        mock.patch.object(PackageVersionHandler, "__enter__"),
        mock.patch.object(PackageVersionHandler, "__exit__"),
        mock.patch.object(PackageVersionHandler, "load_toml_file"),
        mock.patch.object(PackageVersionHandler, "write_toml_file"),
        mock.patch.object(PackageVersionHandler, "get_project_toml"),
    ):
        yield PackageVersionHandler(toml_filepath="")


def test_select_versions(package_version_handler):
    package_version_handler._available_candidates = {
        "core": {"a": [Version("1.0"), Version("2.0")], "b": [Version("0.2"), Version("2.5")]},
        "dev": {
            "c": [Version("1.0.5"), Version("2.5.1")],
            "d": [Version("0.2.3"), Version("2.5.5")],
        },
    }
    assert package_version_handler._select_versions("min") == {
        "core": {"a": Version("1.0"), "b": Version("0.2")},
        "dev": {"c": Version("1.0.5"), "d": Version("0.2.3")},
    }
    assert package_version_handler._select_versions("max") == {
        "core": {"a": Version("2.0"), "b": Version("2.5")},
        "dev": {"c": Version("2.5.1"), "d": Version("2.5.5")},
    }


def test_package_dependencies_from_toml(package_version_handler):
    package_version_handler.get_project_toml.return_value = {
        "project": {
            "dependencies": ["a==2.0", "b==2.5"],
            "optional-dependencies": {"dev": ["c==2.5.1", "d==2.5.5"]},
        }
    }
    package_version_handler._pkg_dep = {}
    assert package_version_handler.get_package_dependencies_from_toml(
        include_optional_dep=False
    ) == {"core": ["a==2.0", "b==2.5"]}
    package_version_handler._pkg_dep = {}
    assert package_version_handler.get_package_dependencies_from_toml(
        include_optional_dep=True
    ) == {"core": ["a==2.0", "b==2.5"], "dev": ["c==2.5.1", "d==2.5.5"]}


def test_update_candidate(package_version_handler):
    name_space = namedtuple("tmp", ["version"])
    with mock.patch.object(
        package_version_handler.repo, "find_all_candidates"
    ) as mock_find_all_candidates:
        mock_find_all_candidates.return_value = [
            name_space(Version(f"{1+ entry/10}")) for entry in range(0, 10)
        ]
        assert package_version_handler.update_candidate("a~=2.0") == {"a": []}
        assert package_version_handler.update_candidate("a~=1.9") == {"a": [Version("1.9")]}
